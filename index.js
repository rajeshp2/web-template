const express = require('express');
const app = express();
app.use(express.json());
const PORT = process.env.PORT || 8000;
require('./app')(app);

app.listen(PORT, async () => {
    console.log(`
    ###########################################
     🚀 Server's listening on port: ${PORT} 🚀`);
    console.log(`           
    ###########################################`)
})