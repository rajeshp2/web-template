const express = require('express');
const app = express();
const { default: mongoose } = require('mongoose');
const db = require('./src/db/db')
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const errorHandler = require('./src/helper/error-handler');

const template = require('./src/module/web-templates/template-route')

db.on('open', () => {
    {
        console.log(`
    ###########################################
    `);
        console.log(`         ✅"Connected to mongo server." ✅
    ###########################################`)
    }
})

db.on('error', (err) => {
    console.log("Could not connect to mongo server!");
    return console.log(err);
})

module.exports = (app) =>{
    app.use('/api/template', template)

    app.use('/api-prefix/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
	
	// Project level error handling
	//	app.use(error); // (ALWAYS Keep this as the last route)
	app.use(errorHandler);
}