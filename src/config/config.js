const path = require('path');
const dotenv = require('dotenv');
const envFound = dotenv.config({ path: path.resolve(__dirname, '../../.env') });

// Check if .env file exits
if (envFound.error) {
    // This error should crash whole process
    throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

dotenv.config({ path: path.resolve(__dirname, '../../.env') });  // To get absolute path

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

module.exports = {

    // Enviornment
    NODE_ENV: process.env.NODE_ENV,

    // PORT
    PORT: process.env.PORT,

    //db Credentials
    mongodb : {
        USERNAME : process.env.USERNAME1,
        PASSWORD : process.env.PASSWORD,
        CLUSTER : process.env.CLUSTER,
        DBNAME : process.env.DBNAME
    },

  
}
