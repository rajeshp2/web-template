exports.resSuccess = (message, results, statusCode) => {
    return {
        message,
        error: false,
        code: statusCode,
        results
    };
};

exports.resError = (message, errorStack, statusCode) => {
    // List of common HTTP request code
    const codes = [200, 201, 400, 401, 404, 403, 422, 500];

    // Get matched code
    const findCode = codes.find((code) => code == statusCode);

    if (!findCode) statusCode = 500;
    else statusCode = findCode;

    return {
        message,
        error: true,
        code: statusCode,
        errorStack
    };
};


exports.resValidation = (errors) => {
    return {
        message: "Validation errors",
        error: true,
        code: 422,
        errors
    };
};


module.exports.errorHandler = (errorsObject) => {
	let resObj = { status: 400, message: 'Bad Request' };
	resObj.data = errorsObject.map(errors => {
		if (errors.keyword === 'format') {
			return { field: errors.params.format, message: errors.message };
		} else if (errors.keyword === 'required') {
			return { field: errors.params.missingProperty, message: errors.message };
		}else if(errors.keyword === 'isNotEmpty'){
			return { field: errors.instancePath.replace('/',''), message: errors.instancePath.replace('/','') +' can not be empty' };
		}else if(errors.keyword === 'type'){
			return { field: errors.instancePath.replace('/',''), message: errors.message };
		} else if (errors.keyword === 'additionalProperties') {
			return { field: errors.params.additionalProperty, message: errors.message };
		}

	});
	return resObj;
};