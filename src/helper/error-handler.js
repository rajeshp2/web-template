const errorHandler = (err, req, res, next) => {
	let errObj = {};
	let status = err.status || 500;

	if (res.headersSent) {
		return next(err);
	}

	errObj['stackTrace'] = err.stack;
	errObj['status'] = status;
	errObj['message'] = err.message || 'Internal server error.';

	if (process.env.NODE_ENV != 'development') {
		delete errObj['stackTrace'];
	}

	res.status(status).json(errObj);
};

module.exports = errorHandler;