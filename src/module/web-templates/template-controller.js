const Templates = require('../../model/template')
const { log } = require('../../helper/logger');
const mongoose = require("mongoose");
const { resSuccess, resError, resValidation } = require('../../helper/response');
const Ajv = require('ajv');
const ajv = new Ajv({ allErrors: true });
require('ajv-formats')(ajv);
// const utils=require('../../helper/utils');
// utils.customAjvRule(ajv);
const template_service = require('./template-service')
const apiResp = require('../../helper/response');
const validateAddTemplate = ajv.compile(require('./ajv-schema/add-template.json'))
const validateUpdateTemplate = ajv.compile(require('./ajv-schema/update-template.json'))

exports.addTemplate = async (req, res) => {
try {
    const { templateName, structure } = req.body;

    if (!validateAddTemplate(req.body)) {
        log('error', 'add template Validation Error', 'template-controller:addTemplate');
        return res.status(400).json(apiResp.errorHandler(validateAddTemplate.errors));
    }

    const response = await template_service.addTemplate(templateName, structure);
    switch (response.status) {
    case 'success':
        return res.status(200).json(resSuccess('Template Data Added Successfully.', response.data, res.statusCode));
    default:
        break;
    } 

} catch (error) {
    return res.status(500).json(resError('Error while Adding Template Data', error.stack, res.statusCode));
}
}

exports.getAllTemplates = async (req, res) => {
    try {
        const response = await template_service.getAllTemplates();
        switch (response.status) {
        case 'success':
            return res.status(200).json(resSuccess('Templates Fetch succesfully', response.data, res.statusCode));
        default:
            break;
        } 
      
    } catch (error) {
        console.log(error);
        return res.status(500).json(resError('Failed to fetch all templates', error.stack, res.statusCode));
    }
}

exports.getTemplateById = async (req, res) => {
    try {
        const { id } = req.query
        if (!id) {
            return res.status(400).json(resValidation('Please Enter id', {}, res.statusCode));
        }
        const response = await template_service.getTemplateById(id);
        switch (response.status) {
        case 'success':
            return res.status(200).json(resSuccess(' Template Fetch succesfully', response.data, res.statusCode));
        default:
            break;
        } 
       
    } catch (error) {
        console.log(error);
        return res.status(500).json(resError('Failed to fetch template by id', error.stack, res.statusCode));
    }
}

exports.updateTemplate = async (req, res) => {
    try {
        const {id} = req.query
        const updateBody = req.body;
        if (!id) {
            return res.status(400).json(resValidation('Please Enter id', {}, res.statusCode));
        }
        if (!validateUpdateTemplate(req.body)) {
            log('error', 'update template Validation Error', 'template-controller:updateTemplate');
            return res.status(400).json(apiResp.errorHandler(validateUpdateTemplate.errors));
        }

        const response = await template_service.updateTemplate(id, updateBody);
        switch (response.status) {
        case 'success':
            return res.status(200).json(resSuccess('Template Data Updateded succesfully', response.data, res.statusCode));
        default: 
            break;
        } 
    } catch (error) {
        console.log(error);
        return res.status(500).json(resError('Failed to Update Template', error.stack, res.statusCode));
    }
}

exports.deleteTemplate = async (req, res) => {
    try {
        const { id } = req.query
        if (!id) {
            return res.status(400).json(resValidation('Please Enter id', {}, res.statusCode));
        }
        const response = await template_service.deleteTemplate(id);
        switch (response.status) {
        case 'success':
            return res.status(200).json(resSuccess('Template Deleted succesfully', response.data, res.statusCode));
        default: 
            break;
        } 
       
    } catch (error) {
        console.log(error);
        return res.status(500).json(resError('Failed to Delete Template', error.stack, res.statusCode));
    }
}