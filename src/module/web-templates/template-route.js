const express = require('express')
const router = express.Router();
const template_controller = require('./template-controller');

router.post('/add_template',template_controller.addTemplate)
router.get('/get_all_templates',template_controller.getAllTemplates)
router.get('/get_template_by_id',template_controller.getTemplateById)
router.put('/update_template',template_controller.updateTemplate)
router.delete('/delete_template',template_controller.deleteTemplate)

module.exports = router