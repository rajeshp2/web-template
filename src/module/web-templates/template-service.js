const Templates = require('../../model/template')
const mongoose = require("mongoose");

exports.addTemplate = async (templateName, structure) => {

    let templateData = await Templates({
        _id: new mongoose.Types.ObjectId(),
        templateName,
        structure       
    })
    await templateData.save();
    return  { status: 'success', data: templateData };

}

exports.getAllTemplates = async () => {
    
    const allTemplates = await Templates.find();
    return  { status: 'success', data: allTemplates };
    
}

exports.getTemplateById = async (id) => {
   
    const singleTemplate = await Templates.findOne({_id: id});
    return  { status: 'success', data: singleTemplate };
    
}

exports.updateTemplate = async (id, updateBody) => {
   
        let updateTemplateBody = await Templates.updateOne({ _id: id }, updateBody)
        const data =  { ...updateTemplateBody, ...updateBody }
        return  { status: 'success', data: data};  
    
}

exports.deleteTemplate = async (id) => {

    const deleteTemplate = await Templates.deleteOne({ _id: id })
    return  { status: 'success', data: { ...deleteTemplate }};

}