const mongoose = require('mongoose');
const config = require('../config/config');
const username = config.mongodb.USERNAME;
const password = config.mongodb.PASSWORD;
const cluster = config.mongodb.CLUSTER;
const dbname = config.mongodb.DBNAME;

mongoose.connect(
	`mongodb+srv://${username}:${password}@${cluster}.mongodb.net/${dbname}?retryWrites=true&w=majority`, 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

const db = mongoose.connection;

module.exports = db;