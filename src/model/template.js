const { default: mongoose } = require('mongoose');

 const templateSchema = new mongoose.Schema({

    _id: mongoose.Schema.Types.ObjectId,
    templateName: { type: String, required: true },
    structure: { type: Object, required: true }
 });
 module.exports = mongoose.model("templates", templateSchema);
